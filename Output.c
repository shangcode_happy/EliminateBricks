#include "Output.h"

void Output(int Height,int Brick[][Height],Object *pAll)
  {
    OptBrick(Height, Brick);
    OptBall(pAll);
    OptBoard(pAll);
    OptWall();
    OptScore(pAll);
    pAll->OldBallX = pAll->BallX;
    pAll->OldBallY = pAll->BallY;
  }

void OptBrick(int Height, int Brick[][Height])
  {
    for ( int X = 1; X < 50; X++)
      {
        for ( int Y = 1; Y <= 10; Y++)
          {
            if (Brick[X][Y] == 1)//Haven't been hit
              OptOneBrick(X, Y);
            if (Brick[X][Y] == 2)//Eliminated by ball
              Erase(X, Y);//Erase old graph
          }
      }
    WhiteBlack();
  }
void OptOneBrick(int X, int Y)
  {
    ColorFul(Y);//Looks like a brick
    GoToXY(X, Y);//Locate
    printf("c");//Actually invisible
  }
void ColorFul(int Y)
  {
    if (Y == 0)
      GreyBrick();//Use color skill to output brick
    if (Y == 1)
      PurplBrick();
    if (Y == 2)
      GreenBrick();
    if (Y == 3)
      YellowBrick();
    if (Y == 4)
      WhiteBrick();
    if (Y == 5)
      WhiteBrick();
    if (Y == 6)
      YellowBrick();
    if (Y == 7)
      GreenBrick();
    if (Y == 8)
      PurplBrick();
    if (Y == 9)
      GreyBrick();
    if (Y == 10)
      GreenBrick();
  }
void Erase(int X, int Y)
  {
    WhiteBlack();
    GoToXY(X, Y);//Locate
    printf(" ");//Wipe
  }

void OptWall()
  {
    for ( int Y = 1; Y < 18; Y++)
      {
        RedRed();
        LeftWall(Y);
        RightWall(Y);
      }
    WhiteBlack();
  }
void LeftWall(int Y)
  {
    GoToXY(0, Y);
    printf("|");
  }
void RightWall(int Y)
  {
    GoToXY(50, Y);
    printf("|");
  }

void OptBall(Object *pAll)
  {
    int BallX = pAll->BallX, BallY = pAll->BallY;
    WipeOldBall(pAll);
    GoToXY(BallX, BallY);
    printf("o");
  }
void WipeOldBall(Object *pAll)
  {
    int OldBallX = pAll->OldBallX, OldBallY = pAll->OldBallY;
    GoToXY(OldBallX, OldBallY);
    printf(" ");
  }

void OptBoard(Object *pAll)
  {//Over the length I set
    int BoardX = pAll->BoardX;
    int BottomArr[50], *pBArr = BottomArr;
    RecordBoard(pBArr, BoardX);
    for ( int X = 1; X < 50; X++)
      {
        if (BottomArr[X] == 1)
          {
            GoToXY(X, 18);
            BlueBlue();
            printf("=");
            WhiteBlack();
          }
        else
          {
            GoToXY(X, 18);
            printf(" ");
          }
      }
  }
void RecordBoard(int *pBArr, int BoardX)
  {
    if ((BoardX > 0 ) && ( (BoardX + 6) <= 50))
      {
        for ( int X = 0; X < BoardX - 6; X++)//Refresh
          pBArr[X] = 0;
        for ( int X = BoardX-6; X <= BoardX+6; X++)//Board
          pBArr[X] = 1;
        for ( int X = BoardX+7; X < 50; X++)
          pBArr[X] = 0;
    }
  }

void OptScore(Object *pAll)
  {
    GoToXY(2, 16);
    int Score = pAll->Score;
    YellowBlack();
    printf("Your score:%d\n", Score);
    WhiteBlack();
  }
