#ifndef _OUTPUT_
#define _OUTPUT_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Color.h"
#include "Game.h"
#include "Menu.h" 
void Output(int Height,int Brick[][10], Object *pAll);

void OptBrick(int Height, int Brick[][10]);
void OptOneBrick(int X, int Y);
void ColorFul(int Y);
void Erase(int X, int Y);

void OptWall();
void LeftWall(int Y);
void RightWall(int Y);

void OptBall(Object *pAll);
void WipeOldBall(Object *pAll);

void OptBoard(Object *pAll);
void RecordBoard(int *pBArr, int boa);


void OptScore(Object *pAll);

#endif
