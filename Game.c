#include "Game.h"

void Game(PageStatus *pStat)
  {
    StartGameSound();
    ClearScreen();//Get in game from menu
    Object All, *pAll = &All;//A Struct record everything
    InitGame(pAll);//Init some parameter

    int Width = pAll->Width, Height = pAll->Height;
    int Brick[Width][Height];
    InitBrick(Height, Brick, pAll);

    int Continue = 1;
    while (Continue)
      {
        PlayerAct(pAll, &Continue);
        MoveBall(pAll);
        HitBrick(Height, Brick, pAll);
        Lose(pAll, &Continue);
        Output(Height, Brick, pAll);
      }
    ClearScreen();
    pStat->Enter = 0;
    pStat->FinalScore = pAll->Score;
    pStat->Played = 1;
    GameOverSound();
  }//Transfer Score to show on menu

void InitGame(Object *pAll)
  {
    int Width = 50, Height = 11;//For brick
    pAll->Width = Width, pAll->Height = Height;
    pAll->Score = 0;
    InitBoard(pAll);
    InitBall(pAll);
  }
void InitBoard(Object *pAll)
  {
    pAll->BoardX = 23;//almost on the center of game screen
    pAll->BoardY = 18;//On the bottom
  }
void InitBall(Object *pAll)
  {
    pAll->CouldX = 0;
    pAll->BallX = 23, pAll->BallY = 12;
    pAll->AccX = 0, pAll->AccY = 1;
  }
void InitBrick(int Height, int Brick[][Height], Object *pAll)
  {
    for ( int X = 1; X < 50; X++)
        for ( int Y = 1; Y <= 10; Y++)
            Brick[X][Y] = 1;//Brick exist
  }

void PlayerAct(Object *pAll, int *Continue)
  {
    char Key;
    if (kbhit())
      Key = getch();

    if (Key == 75)//Left
      pAll->BoardX = pAll->BoardX - 1;
    if (Key == 77)//Right
      pAll->BoardX = pAll->BoardX + 1;
    if (Key == ' ')//ShootBall, unfinished
      pAll->Server = 1;
    if (Key == 113)//q
      *Continue = 0;
    if (Key == 27)//ESC
      *Continue = 0;
  }

void MoveBall(Object *pAll)
  {
    pAll->CouldX = pAll->CouldX + 1;
    if (pAll->CouldX == 3)
      {
        pAll->BallX = pAll->BallX + pAll->AccX;
        pAll->BallY = pAll->BallY + pAll->AccY;
        pAll->CouldX = 0;
      }
    HitWall(pAll);
    HitBoard(pAll);
  }
void HitWall(Object *pAll)
  {
    int BallX = pAll->BallX, BallY = pAll->BallY;
    int LeftWall = 0, RightWall = 49;
    int Top = 0;
    if ((BallX <= LeftWall) || (BallX >= RightWall))
        LRRebound(pAll);
    if (BallY <= Top)
        TopRebound(pAll);
  }
void LRRebound(Object *pAll)
  {//just reverse the accelerate speed from wall
    int AccX = pAll->AccX;
    pAll->AccX = - pAll->AccX;
  }
void TopRebound(Object *pAll)
  {
    pAll->BallY = 1;
    pAll->AccY = 1;
  }

void HitBoard(Object *pAll)
  {
    int BallY = pAll->BallY, BoardY = pAll->BoardY;
    int Distance = abs(pAll->BallX - pAll->BoardX);
    // The Distance on horizantal between ball and board
    if (Distance <= 6)//Board could catch ball
      {
        if (BallY == 18)//Lack a =!!!
            BoardRebound(pAll);
      }
  }
void BoardRebound(Object *pAll)
  {
    int RawDistance = pAll->BallX - pAll->BoardX;
    if ((RawDistance <= 1)&&(RawDistance >= -1))//see hit left side or
      {
        pAll->AccX = 0;//right side of Board
        pAll->AccY =  -1;
      }
    if ((RawDistance >= -6) && (RawDistance < -1))//hit Left board
      {
        pAll->AccX = -1;
        pAll->AccY =  -1;
      }
    if ((RawDistance > 1) && (RawDistance <= 6))//hit Right board
      {
        pAll->AccX = 1;
        pAll->AccY =  -1;
      }
  }

void HitBrick(int Height, int Brick[][Height], Object *pAll)
  {
    int X = pAll->BallX, Y = pAll->BallY;
    if ((X > 0) && (X < 50))
      {//Check if ball still in the game screen
        if ((Y > 0) && (Y <= 10))
          {//Check if ball is on the Brick range
            if (Brick[X][Y] == 1)
            {
              Brick[X][Y] = 2;//Mark brick has been eliminate
              BrickRebound(pAll);
              HitBrickSound();
              pAll->Score = pAll->Score + 1;
            }
          }
      }

}
void BrickRebound(Object *pAll)
  {
    pAll->AccY = 1;
  }

void Lose(Object *pAll, int *Continue)
  {//Exit Game, back to the menu
    int Distance = abs(pAll->BallX - pAll->BoardX);
    int NextBallY = pAll->BallY + pAll->AccY;
    // printf("Distance %d", Distance);
    if (NextBallY > 18)
      if (Distance > 6)
        {
            *Continue = 0;//Exit game to menu
            // Debug(pAll);
        }
  }
void Debug(Object *pAll)
  {
    GoToXY(0,0);//debug
    printf("AccX = %d ", pAll->AccX);
    // int Distance = pAll->BallX - pAll->BoardX;
    printf("ballY= %d ", pAll->BallY);
    printf("ballX= %d ", pAll->BallX);
    // printf("BoardX = %d ", pAll->BoardX);
    // printf("BoardY = %d", BoardY);
    getchar();
  }

// JinX
// Wednesday, August 19, 2020 1:00 PM
