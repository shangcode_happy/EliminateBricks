#ifndef _GAME_
#define _GAME_

#include <stdio.h>
#include "WinAPI.h"

typedef struct//Object
 {
    int BallX, BallY;//Coordinate of Ball
    int CouldX;
    int OldBallX, OldBallY;//Old graph
    int AccX, AccY;//used to change Ball's Coordinate
    int Server;//unfinished
    int BoardX, BoardY;//Coordinate of Board
    int Width, Height;//Of Brick
    int LeftWall, RightWall;//Coordinate of wall
    int Score;//
 }Object;

#include "Menu.h"

void Game(PageStatus *pStat);
void InitGame(Object *pAll);
void InitBall(Object *pAll);
void InitWall(Object *pAll);
void InitBoard(Object *pAll);
void InitBrick(int Height, int BrickArr [][10], Object *pAll);

void PlayerAct(Object *pAll, int *Continue);

void MoveBall(Object *pAll);

void HitWall(Object *pAll);
void HitBoard(Object *pAll);
void LRRebound(Object *pAll);
void TopRebound(Object *pAll);

void HitBoard(Object *pAll);
void BoardRebound(Object *pAll);

void Rebound(Object *pAll);
void HitBrick(int Height, int Brick[][10], Object *pAll);
void BrickRebound(Object *pAll);

void Lose(Object *pAll, int *Continue);

void Debug(Object *pAll);
#endif
